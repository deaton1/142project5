import java.util.Arrays;

public class HashIntSet {

	private class Node{
		public int data;
		public Node next;

		public Node(int value) {
			data = value;
			next = null;
		}
		public Node(int value, Node next) {
			data = value;
			this.next = next;
		}
	}

	private Node[] elements;
	private int size;
	public HashIntSet() {
		elements = new Node[10];
		size = 0;
	}

	public int hash(int i) {
		return (Math.abs(i) % elements.length);
	}

	public void add(int value) {
		if(!contains(value)) {
			int h = hash(value);
			Node newNode = new Node(value);
			newNode.next = elements[h];
			elements[h] = newNode;
			size++;

		}
	}
	public boolean contains(int value) {
		Node current = elements[hash(value)];
		while(current != null) {
			if(current.data == value) {
				return true;
			}
			current = current.next;
		}
		return false;
	}

	public String toString() {
		String s = "";
		for(Node n:elements) {
			Node current = n;
			while(current != null) {
				s += current.data + " ";
				current = current.next;
			}
		}
		return s;
	}

	public void remove(int value) {
		int h = hash(value);
		if(elements[h] != null && elements[h].data == value) {
			elements[h] = elements[h].next;
			size--;
		}else {
			Node current = elements[h];
			while(current != null && current.next != null) {
				if(current.next.data == value) {
					current.next = current.next.next;
					size--;
					return;
				}
				current = current.next;
			}
		}
	}

	public void addAllHashIntSet(HashIntSet his) {
		for (Node element : his.elements) {
			Node current = element;

			while (current != null) {
				add(current.data);
				current = current.next;
			}
		}
	}

	public void removeAllHashIntSet(HashIntSet his) {
		for (Node element : his.elements) {
			Node current = element;

			while (current != null) {
				remove(current.data);
				current = current.next;
			}
		}
	}

	public boolean equalsAllHashIntSet(HashIntSet his) {
		if (his.size != size) return false;

		for (Node element : his.elements) {
			Node current = element;

			while (current != null) {
				if (!contains(current.data)) {
					return false;
				}

				current = current.next;
			}
		}

		return true;
	}

	public void retainAllHashIntSet(HashIntSet his) {
		for (Node element : elements) {
			Node current = element;

			while (current != null) {
				if (!his.contains(current.data)) {
					remove(current.data);
				}

				current = current.next;
			}
		}
	}

	public int[] toArrayHashIntSet() {
		int idx = 0;
		int[] array = new int[size];

		for (Node element : elements) {
			Node current = element;

			while (current != null) {
				array[idx] = current.data;
				current = current.next;
				idx++;
			}
		}

		return array;
	}

	public static void main(String[] args) {
		HashIntSet set = new HashIntSet();
		set.add(37);
		set.add(-2);
		set.add(49);
		set.add(47);
		set.add(57);

		System.out.println(set);
//		System.out.println(set.contains(57));
//		set.remove(7);
//		System.out.println(set);

		HashIntSet s = new HashIntSet();
		s.add(3);
		s.add(10);
		s.add(49);
		s.add(47);
		s.add(589);
		set.addAllHashIntSet(s);
		System.out.println(set);
	}

}